<?php
/**
 * Created by PhpStorm.
 * User: Aleksandar
 * Date: 05-Aug-15
 * Time: 13:39
 */

namespace Business\DTO;


class SenderDTO {

    public $Name;
    public $Address;

    public function __construct($name = null, $address = null) {
        $this->Name = $name;
        $this->Address = $address;
    }

}