<?php
namespace Business\ApiControllers;

use Business\Models\UserModel;
use Business\Security\Crypt;
use Business\Security\Users;
use Data\DataManagers\ConfirmationLinksDataManager;
use Data\DataManagers\UsersDataManager;
use Data\DataManagers\UserRolesDataManager;

class UsersApiController {

	public static function InsertUser($model) {
		return UsersDataManager::InsertUser($model);
	}

	public static function UpdateUser($model) {
		return UsersDataManager::UpdateUser($model);
	}

	public static function DeleteUser($userId) {
		return UsersDataManager::DeleteUser($userId);
	}

	/**
	 * @return UserModel[]
	 */
	public static function GetUsers() {
		return UsersDataManager::GetUsers();
	}

	public static function CountUsers() {
		return UsersDataManager::CountUsers();
	}

	public static function CountAdmins() {
		return UsersDataManager::CountAdmins();
	}

	/**
	 * @return UserModel[]
	 */
	public static function GetAdmins() {
		return UsersDataManager::GetAdmins();
	}

	/**
	 * @param $userId
	 * @return UserModel
	 */
	public static function GetUser($userId) {
		return UsersDataManager::GetUser($userId);
	}

	public static function GetUserByUsername($userName) {
		return UsersDataManager::GetUserByUsername($userName);
	}

	public static function GetUserRole($userId) {
		return UserRolesDataManager::GetUserRole($userId);
	}


	public static function InsertUserRole($model) {
		return UserRolesDataManager::InsertUserRole($model);
	}

	public static function DeleteUserRole($userRoleId) {
		UserRolesDataManager::DeleteUserRole($userRoleId);
	}

	public static function InsertPasswordResetLink($passwordResetLink) {
		return UsersDataManager::InsertPasswordResetLink($passwordResetLink);
	}

	public static function GetPasswordResetLink($token) {
		return UsersDataManager::GetPasswordResetLink($token);
	}

	public static function CheckLogin($email = null, $password = null, $token = null) {
		if ($token == null) {
			return Users::CheckEmailAndPassword($email, $password);
		} else {
			return Users::CheckToken($token);
		}
	}

	public static function CheckLoginAdmin($username = null, $password = null, $token = null) {
		if ($token == null) {
			return Users::CheckUsernameAndPassword($username, $password);
		} else {
			return Users::CheckToken($token);
		}
	}

	public static function Register($name, $email, $username, $password, $image, $roleId, $confirmRegistration = 0, $active = 0){
		return Users::Register($name, $email, $username, $password, $image, $roleId, $confirmRegistration, $active);
	}

	public static function GetActiveToken($userId, $accessTokenTypeId) {
		return UsersDataManager::GetUserAccessToken($userId, $accessTokenTypeId);
	}


	public static function RemoveUserAccessToken($userId, $accessTokenTypeId) {
		return UsersDataManager::RemoveUserAccessToken($userId, $accessTokenTypeId);
	}

	public static function CreateUserAccessToken($userId, $accessTokenTypeId) {
		return UsersDataManager::CreateUserAccessToken($userId, $accessTokenTypeId);
	}


	public static function GetRolePermissions($roleId) {
		return UsersDataManager::GetRolePermissions($roleId);
	}

	public static function GetUserPermissions($userId) {
		return UsersDataManager::GetUserPermissions($userId);
	}

	public static function CryptPassword($password) {
		return Crypt::HashPassword($password);
	}

	public static function UpdateUserRole($userRole) {
		return UsersDataManager::UpdateUserRole($userRole);
	}

	//region ConfirmationLinks
	public static function CreateConfirmationLink($model) {
		return ConfirmationLinksDataManager::Insert($model);
	}

	/**
	 * @param $userId
	 * @return \Business\Models\UserRoleModel[]
	 */
	public static function GetUserRoles($userId) {
		return UserRolesDataManager::GetUserRoles($userId);
	}

	//endregion

	public static function GetUserByEmail($email) {
		return UsersDataManager::GetUserByEmail($email);
	}

	public static function UpdatePasswordResetLink($model) {
		UsersDataManager::UpdatePasswordResetLink($model);
	}

	public static function ActivateUser($userId) {
		$user = UsersApiController::GetUser($userId);
		$user->ConfirmRegistration = 1;
		$user->Active = 1;

		UsersApiController::UpdateUser($user);
	}

	/**
	 * @param $guid
	 * @return \Business\Models\ConfirmationLinkModel
	 */
	public static function GetConfirmationLinkByGuid($guid) {
		return ConfirmationLinksDataManager::GetByGuid($guid);
	}

	/**
	 * @param $guid
	 */
	public static function GetUserResetPasswordLink($guid) {
		return UsersDataManager::GetUserResetPasswordLink($guid);
	}

	public static function GetUserResetPasswordLinkByUserId($userId) {
		return UsersDataManager::GetUserResetPasswordLinkByUserId($userId);
	}

	public static function DeleteUserResetPasswordLink($PasswordResetLinkId) {
		return UsersDataManager::DeleteUserResetPasswordLink($PasswordResetLinkId);
	}
	// endregion

}