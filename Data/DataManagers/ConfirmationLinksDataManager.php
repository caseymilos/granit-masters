<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 30.5.2016.
 * Time: 16.42
 */

namespace Data\DataManagers;


use Data\Repositories\ConfirmationLinksRepository;

class ConfirmationLinksDataManager
{

	public static function GetByGuid($guid){
		return ConfirmationLinksRepository::GetOne(["ConfirmationLink" => $guid]);
	}

	public static function Insert($model)
	{
		return ConfirmationLinksRepository::Insert($model);
	}}