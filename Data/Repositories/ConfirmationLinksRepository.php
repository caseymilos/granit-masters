<?php
/**
 * Created by PhpStorm.
 * User: Milos
 * Date: 30.5.2016.
 * Time: 16.27
 */

namespace Data\Repositories;
use Business\Models\ConfirmationLinkModel;

/**
 * Class ConfirmationLinksRepository
 * @package Data\Repositories
 * @method static ConfirmationLinkModel[] Get
 * @method static ConfirmationLinkModel GetOne
 */

class ConfirmationLinksRepository extends BaseRepository {

}