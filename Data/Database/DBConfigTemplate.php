<?php

namespace Data\Database;


use Data\Database\Protocol\ConnectionDetails;

class DBConfig {

    public static function GetConnections() {
        return [
            "default" => new ConnectionDetails("granit"),
        ];
    }

    /**
     * @param $connection
     * @return ConnectionDetails
     */
    public static function GetConnection($connection) {
        $connections = self::GetConnections();
        if(isset($connections[$connection])) {
            return $connections[$connection];
        }
        else {
            return $connections['default'];
        }
    }

}