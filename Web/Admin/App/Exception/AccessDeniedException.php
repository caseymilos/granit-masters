<?php


class AccessDeniedException extends MVCException {

    public function DisplayError() {

        $controller = new ErrorController(false);
        $controller->AccessDenied();

    }
}