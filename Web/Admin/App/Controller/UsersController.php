<?php


use Business\ApiControllers\UsersApiController;
use Business\Enums\PermissionsEnum;
use Business\Enums\RolesEnum;
use Business\Models\UserModel;
use Business\Models\UserRoleModel;
use Business\Security\Crypt;

class UsersController extends MVCController {

	public function GetUsers($permissions = [PermissionsEnum::ViewUsers]) {
		$model = new UsersViewModel();
		$model->Users = UsersApiController::GetUsers();

		$this->RenderView("Users/Users", ["model" => $model]);
	}

	public function GetAddUser($permissions = [PermissionsEnum::AddUsers]) {
		$model = new UserViewModel();

		$this->RenderView("Users/AddUser", ["model" => $model]);
	}

	public function PostAddUser($username, $email, $name, $password, $active = false, $image = null, $permissions = [PermissionsEnum::AddUsers]) {
		$newImageName = null;
		if ($image['name'] !== "") {
			$newImageName = self::_generateImageName($image['name'], $name);
			move_uploaded_file($image['tmp_name'], self::_generateImageFullPath($newImageName));
		}

		$newUser = UsersApiController::Register($name, $email, $username, $password, $newImageName, RolesEnum::Admin, 1, (bool)$active);

		Router::Redirect("users");
	}

	public function GetEditUser($userId, $permissions = [PermissionsEnum::EditUsers]) {
		$model = new UserViewModel();
		$model->User = UsersApiController::GetUser($userId);

		$this->RenderView("Users/EditUser", ["model" => $model]);
	}

	public function PostEditUser($userId, $username, $email, $name, $password, $active = false, $image = null, $permissions = [PermissionsEnum::EditUsers]) {
		$user = UsersApiController::GetUser($userId);

		$user->Username = $username;
		$user->Email = $email;
		$user->Name = $name;
		if ($password != "") {
			$user->Password = Crypt::HashPassword($password);
		}
		$user->Active = (bool)$active;

		if ($image['name'] !== "") {
			$newName = self::_generateImageName($image['name'], $name);
			move_uploaded_file($image['tmp_name'], self::_generateImageFullPath($newName));
			$user->Image = $newName;
		}

		UsersApiController::UpdateUser($user);

		Router::Redirect("users");
	}

	public function GetDeleteUser($userId, $permissions = [PermissionsEnum::DeleteUsers]) {
		$success = UsersApiController::DeleteUser($userId);

		Router::Redirect("users");
	}

	private static function _generateImageName($image, $name) {
		return CommonHelper::StringToFilename(sprintf("user-%s-%s.%s", $name, CommonHelper::GenerateRandomString(), CommonHelper::GetExtension($image)));
	}

	private static function _generateImageFullPath($imageName) {
		return sprintf("%s/Media/Users/%s", CDN_PATH, $imageName);
	}

}