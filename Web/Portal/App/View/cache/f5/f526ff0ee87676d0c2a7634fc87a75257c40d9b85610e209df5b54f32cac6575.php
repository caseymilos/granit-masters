<?php

/* Home/Home.twig */
class __TwigTemplate_ae14f0a1d35820b6f3f03b66d55e9a8aeb87e9b692ab7aaabd41a632e92fc60c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("Master/Master.twig", "Home/Home.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'head' => array($this, 'block_head'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "Master/Master.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Home";
    }

    // line 5
    public function block_head($context, array $blocks = array())
    {
        // line 6
        echo "
";
    }

    // line 10
    public function block_content($context, array $blocks = array())
    {
        // line 11
        echo "\t<h1 style=\"text-align: center; margin-bottom: 50px;\">HOMEPAGE</h1>

\t";
        // line 13
        if (($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "IsLoggedIn", array(), "method") == true)) {
            // line 14
            echo "\t\t";
            if (twig_length_filter($this->env, $this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Users", array()))) {
                // line 15
                echo "\t\t\t";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["model"]) ? $context["model"] : null), "Users", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["user"]) {
                    // line 16
                    echo "\t\t\t\t<div class=\"row user-row\">
\t\t\t\t\t<div class=\"col-lg-3\">
\t\t\t\t\t\t<img src=\"";
                    // line 18
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "GetImageUrl", array(), "method"), "html", null, true);
                    echo "\"/>
\t\t\t\t\t</div>
\t\t\t\t\t<div class=\"col-lg-3\">";
                    // line 20
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "Name", array()), "html", null, true);
                    echo "</div>
\t\t\t\t\t<div class=\"col-lg-3\">";
                    // line 21
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "Email", array()), "html", null, true);
                    echo "</div>
\t\t\t\t\t<div class=\"col-lg-3\">";
                    // line 22
                    echo twig_escape_filter($this->env, $this->getAttribute($context["user"], "RegistrationDate", array()), "html", null, true);
                    echo "</div>
\t\t\t\t</div>
\t\t\t";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['user'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 25
                echo "\t\t";
            }
            // line 26
            echo "\t";
        } else {
            // line 27
            echo "\t\t<p style=\"text-align: center;\">Log in to see list of users!</p>
\t";
        }
    }

    public function getTemplateName()
    {
        return "Home/Home.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 27,  90 => 26,  87 => 25,  78 => 22,  74 => 21,  70 => 20,  65 => 18,  61 => 16,  56 => 15,  53 => 14,  51 => 13,  47 => 11,  44 => 10,  39 => 6,  36 => 5,  30 => 3,  11 => 1,);
    }
}
/* {% extends "Master/Master.twig" %}*/
/* */
/* {% block title %}Home{% endblock %}*/
/* */
/* {% block head %}*/
/* */
/* {% endblock %}*/
/* */
/* */
/* {% block content %}*/
/* 	<h1 style="text-align: center; margin-bottom: 50px;">HOMEPAGE</h1>*/
/* */
/* 	{% if Helper.IsLoggedIn() == true %}*/
/* 		{% if model.Users | length %}*/
/* 			{% for user in model.Users %}*/
/* 				<div class="row user-row">*/
/* 					<div class="col-lg-3">*/
/* 						<img src="{{ user.GetImageUrl() }}"/>*/
/* 					</div>*/
/* 					<div class="col-lg-3">{{ user.Name }}</div>*/
/* 					<div class="col-lg-3">{{ user.Email }}</div>*/
/* 					<div class="col-lg-3">{{ user.RegistrationDate }}</div>*/
/* 				</div>*/
/* 			{% endfor %}*/
/* 		{% endif %}*/
/* 	{% else %}*/
/* 		<p style="text-align: center;">Log in to see list of users!</p>*/
/* 	{% endif %}*/
/* {% endblock %}*/
