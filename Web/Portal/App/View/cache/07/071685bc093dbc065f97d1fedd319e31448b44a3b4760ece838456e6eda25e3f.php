<?php

/* Master/Master.twig */
class __TwigTemplate_7067c528c0784987c8d1dd078566683fa583c760559de12c5948a9288f5b1eac extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'og_meta' => array($this, 'block_og_meta'),
            'head_css' => array($this, 'block_head_css'),
            'body_class' => array($this, 'block_body_class'),
            'content' => array($this, 'block_content'),
            'footer_scripts' => array($this, 'block_footer_scripts'),
            'help_script' => array($this, 'block_help_script'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>
<!--[if lt IE 7]>
<html class=\"no-js lt-ie9 lt-ie8 lt-ie7\" lang=\"\"> <![endif]-->
<!--[if IE 7]>
<html class=\"no-js lt-ie9 lt-ie8\" lang=\"\"> <![endif]-->
<!--[if IE 8]>
<html class=\"no-js lt-ie9\" lang=\"\"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class=\"no-js\" lang=\"en-US\">
<!--<![endif]-->

<head>
\t<meta charset=\"utf-8\">
\t<base href=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_constant("Config::baseurl"), "html", null, true);
        echo "\"/>
\t<meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
\t<title>";
        // line 16
        $this->displayBlock('title', $context, $blocks);
        echo " - Ubrium Framework</title>
\t<meta name=\"description\"
\t\t  content=\"Object-oriented, MVC, PHP Framework\">
\t<meta name=\"keywords\" content=\"object oriented,mvc,php,framework\"/>
\t<meta name=\"author\" content=\"Ubrium Galactic LLC\"/>
\t<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">

\t";
        // line 23
        $this->displayBlock('og_meta', $context, $blocks);
        // line 24
        echo "
\t<link rel=\"shortcut icon\" href=\"\" type=\"image/x-icon\">

\t<!-- css section -->
\t<link rel=\"stylesheet\" href=\"Theme/bootstrap/css/bootstrap.min.css\"/>
\t<link rel=\"stylesheet\" href=\"Theme/css/style.css?v=0.0.1\"/>

\t";
        // line 31
        $this->displayBlock('head_css', $context, $blocks);
        // line 33
        echo "</head>
<body class=\"";
        // line 34
        $this->displayBlock('body_class', $context, $blocks);
        echo "\">
<div class=\"header\">
\t<div class=\"logo-holder\">
\t\t<a href=\"";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "home"), "method"), "html", null, true);
        echo "\">
\t\t\t<img class=\"logo\" src=\"Theme/Media/ubrium.png\"/>
\t\t</a>
\t</div>

\t<div class=\"user-holder\">
\t\t";
        // line 43
        if (($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "IsLoggedIn", array(), "method") == true)) {
            // line 44
            echo "\t\t\tHi, ";
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method"), "Name", array()), "html", null, true);
            echo "
\t\t\t<a href=\"";
            // line 45
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "edit-profile"), "method"), "html", null, true);
            echo "\">
\t\t\t\t<img class=\"user-image\" src=\"";
            // line 46
            echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["Helper"]) ? $context["Helper"] : null), "GetCurrentUser", array(), "method"), "Image", array()), "html", null, true);
            echo "\"/>
\t\t\t</a>
\t\t\t<a class=\"btn btn-danger\" href=\"";
            // line 48
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "logout"), "method"), "html", null, true);
            echo "\">Logout</a>
\t\t";
        } else {
            // line 50
            echo "\t\t\t<a class=\"btn btn-default\" href=\"";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "login"), "method"), "html", null, true);
            echo "\">Login</a>
\t\t\t<a class=\"btn btn-success\" href=\"";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["Router"]) ? $context["Router"] : null), "Create", array(0 => "registration"), "method"), "html", null, true);
            echo "\">Register</a>
\t\t";
        }
        // line 53
        echo "\t</div>
</div>
";
        // line 55
        $this->displayBlock('content', $context, $blocks);
        // line 58
        echo "
<!-- Javascript -->
<script src=\"Theme/js/jquery.min.js\"></script>
<script src=\"Theme/bootstrap/js/bootstrap.min.js\"></script>
<script src=\"Theme/js/main.js?v=0.0.1\"></script>

";
        // line 64
        $this->displayBlock('footer_scripts', $context, $blocks);
        // line 67
        echo "
";
        // line 68
        $this->displayBlock('help_script', $context, $blocks);
        // line 70
        echo "</body>
</html>";
    }

    // line 16
    public function block_title($context, array $blocks = array())
    {
    }

    // line 23
    public function block_og_meta($context, array $blocks = array())
    {
    }

    // line 31
    public function block_head_css($context, array $blocks = array())
    {
        // line 32
        echo "\t";
    }

    // line 34
    public function block_body_class($context, array $blocks = array())
    {
    }

    // line 55
    public function block_content($context, array $blocks = array())
    {
        // line 56
        echo "
";
    }

    // line 64
    public function block_footer_scripts($context, array $blocks = array())
    {
        // line 65
        echo "
";
    }

    // line 68
    public function block_help_script($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "Master/Master.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 68,  177 => 65,  174 => 64,  169 => 56,  166 => 55,  161 => 34,  157 => 32,  154 => 31,  149 => 23,  144 => 16,  139 => 70,  137 => 68,  134 => 67,  132 => 64,  124 => 58,  122 => 55,  118 => 53,  113 => 51,  108 => 50,  103 => 48,  98 => 46,  94 => 45,  89 => 44,  87 => 43,  78 => 37,  72 => 34,  69 => 33,  67 => 31,  58 => 24,  56 => 23,  46 => 16,  41 => 14,  26 => 1,);
    }
}
/* <!doctype html>*/
/* <!--[if lt IE 7]>*/
/* <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->*/
/* <!--[if IE 7]>*/
/* <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->*/
/* <!--[if IE 8]>*/
/* <html class="no-js lt-ie9" lang=""> <![endif]-->*/
/* <!--[if gt IE 8]><!-->*/
/* <html class="no-js" lang="en-US">*/
/* <!--<![endif]-->*/
/* */
/* <head>*/
/* 	<meta charset="utf-8">*/
/* 	<base href="{{ constant('Config::baseurl') }}"/>*/
/* 	<meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/* 	<title>{% block title %}{% endblock %} - Ubrium Framework</title>*/
/* 	<meta name="description"*/
/* 		  content="Object-oriented, MVC, PHP Framework">*/
/* 	<meta name="keywords" content="object oriented,mvc,php,framework"/>*/
/* 	<meta name="author" content="Ubrium Galactic LLC"/>*/
/* 	<meta name="viewport" content="width=device-width, initial-scale=1">*/
/* */
/* 	{% block og_meta %}{% endblock %}*/
/* */
/* 	<link rel="shortcut icon" href="" type="image/x-icon">*/
/* */
/* 	<!-- css section -->*/
/* 	<link rel="stylesheet" href="Theme/bootstrap/css/bootstrap.min.css"/>*/
/* 	<link rel="stylesheet" href="Theme/css/style.css?v=0.0.1"/>*/
/* */
/* 	{% block head_css %}*/
/* 	{% endblock %}*/
/* </head>*/
/* <body class="{% block body_class %}{% endblock %}">*/
/* <div class="header">*/
/* 	<div class="logo-holder">*/
/* 		<a href="{{ Router.Create("home") }}">*/
/* 			<img class="logo" src="Theme/Media/ubrium.png"/>*/
/* 		</a>*/
/* 	</div>*/
/* */
/* 	<div class="user-holder">*/
/* 		{% if Helper.IsLoggedIn() == true %}*/
/* 			Hi, {{ Helper.GetCurrentUser().Name }}*/
/* 			<a href="{{ Router.Create("edit-profile") }}">*/
/* 				<img class="user-image" src="{{ Helper.GetCurrentUser().Image }}"/>*/
/* 			</a>*/
/* 			<a class="btn btn-danger" href="{{ Router.Create("logout") }}">Logout</a>*/
/* 		{% else %}*/
/* 			<a class="btn btn-default" href="{{ Router.Create("login") }}">Login</a>*/
/* 			<a class="btn btn-success" href="{{ Router.Create("registration") }}">Register</a>*/
/* 		{% endif %}*/
/* 	</div>*/
/* </div>*/
/* {% block content %}*/
/* */
/* {% endblock %}*/
/* */
/* <!-- Javascript -->*/
/* <script src="Theme/js/jquery.min.js"></script>*/
/* <script src="Theme/bootstrap/js/bootstrap.min.js"></script>*/
/* <script src="Theme/js/main.js?v=0.0.1"></script>*/
/* */
/* {% block footer_scripts %}*/
/* */
/* {% endblock %}*/
/* */
/* {% block help_script %}*/
/* {% endblock %}*/
/* </body>*/
/* </html>*/
