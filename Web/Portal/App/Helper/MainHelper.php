<?php
use Business\Enums\LanguageCodesEnum;

class MainHelper {

	public static function IsLoggedIn() {
		return Security::IsLoggedIn();
	}

	public static function GetCurrentUser() {
		return Security::GetCurrentUser();
	}

	public static function EnumDescription($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::Description($value);
	}

	public static function EnumCaption($enum, $value) {
		$type = "Business\\Enums" . '\\' . $enum;
		$enum = new $type();
		return $enum->Caption($value);
	}

	public static function EnumConstant($enum, $constant) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::GetConstant($constant);
	}

	public static function Enum($enum) {
		$type = "Business\\Enums" . '\\' . $enum;
		return $type::enum();
	}

	public static function ActiveButton($controller, $action = false, $class = "active") {
		return HtmlHelper::ActiveButton($controller, $action, $class);
	}

	public static function GetLanguageId() {
		global $router;
		return LanguageCodesEnum::GetConstant($router->Language);
	}
}